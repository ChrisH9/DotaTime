import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decamel'
})
export class DecamelPipe implements PipeTransform {

  public transform(value: string, args?: any): string {
    return value.split(/(?=[A-Z0-9])/).join(' ');
  }
}
