import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMatch } from './imatch';

@Injectable({
  providedIn: 'root'
})
export class OpenDotaService {

  private openDotaUrl: string = 'https://api.opendota.com/api';

  constructor(private http: HttpClient) { }

  public getMatches(accountID: number): Observable<IMatch[]> {
    return this.http.get<IMatch[]>(this.getMatchHistoryUrl(accountID));
  }

  private getMatchHistoryUrl(accountID: number): string {
    return this.openDotaUrl + `/players/${accountID}/matches`;
  }
}

