import { IMatch } from './imatch';

export class Match implements IMatch {
  public assists: number;
  public deaths: number;
  public duration: number;
  public game_mode: number;
  public hero_id: number;
  public kills: number;
  public leaver_status: number;
  public lobby_type: number;
  public match_id: number;
  public party_size: number;
  public player_slot: number;
  public radiant_win: boolean;
  public skill: number;
  public start_time: number;
  public version: number;

  constructor() { }

  public didPlayerWin(): boolean {
    const isRadiant = this.player_slot < 5;
    return isRadiant === this.radiant_win;
  }

  public date(): Date {
    // Multiply by 1000 because date takes ms and start_time is in seconds
    return new Date(this.start_time * 1000);
  }
}
