import { TestBed, inject } from '@angular/core/testing';

import { OpenDotaService } from './open-dota.service';

describe('OpenDotaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpenDotaService]
    });
  });

  it('should be created', inject([OpenDotaService], (service: OpenDotaService) => {
    expect(service).toBeTruthy();
  }));
});
