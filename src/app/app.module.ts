import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OpenDotaService } from './api/open-dota.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DecamelPipe } from './pipes/decamel.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DecamelPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [OpenDotaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
