import { Match } from './api/match';
import { HourReport } from './hour-report';

export class DayReport {
  public matches: Match[] = [];
  public wins: number = 0;
  public hourReports: HourReport[] = [];
  private winrates: number[] = [];

  constructor (public day: string) {
    for (let i = 0; i < 24; i++) {
      this.hourReports[i] = new HourReport();
    }
  }

  public getWinrates(): number[] {
    this.matches.forEach(match => {
      const date: Date = match.date();
      this.hourReports[date.getHours()].matches.push(match);
    });

    for (let i = 0; i < this.hourReports.length; i++) {
      const hourReport: HourReport = this.hourReports[i];

      this.winrates[i] = hourReport.getWinrate();
      this.wins += hourReport.wins;
    }

    return this.winrates;
  }
}
