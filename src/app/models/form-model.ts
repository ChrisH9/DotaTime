import {GameMode} from "../enum/game-mode.enum";
import {LobbyType} from "../enum/lobby-type.enum";

export class FormModel {
  constructor(
    public steamID: number,
    public gameMode: string | GameMode,
    public lobbyType: string | LobbyType,
    public partySize: string | number){
  }
}
