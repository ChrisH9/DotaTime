import { Component } from '@angular/core';
import { OpenDotaService } from './api/open-dota.service';
import { Match } from './api/match';
import { DayReport } from './day-report';
import { IMatch } from './api/imatch';
import {GameMode} from "./enum/game-mode.enum";
import {LobbyType} from "./enum/lobby-type.enum";
import {FormModel} from "./models/form-model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public dayReports: DayReport[] = [];
  public showTable: boolean = false;

  public hours: number[] = [];
  public days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  public formModel: FormModel;

  public gameModes: (string|GameMode)[] = ["Any", GameMode.AllPick, GameMode.CaptainsMode, GameMode.RandomDraft];
  public lobbyTypes: (string|LobbyType)[] = ["Any", LobbyType.Ranked, LobbyType.Normal, LobbyType.BattleCup, LobbyType.Practice];
  public partySizes: (string|number)[] = ["Any", 1, 2, 3, 4, 5];

  public totalMatchesPlayed: number;
  public totalMatchesIncluded: number;

  constructor(private openDotaService: OpenDotaService) {
    this.setupNewReport();
    for (let i: number = 0; i<24; i++) {
      this.hours[i] = i;
    }

    this.formModel = new FormModel(215628405, GameMode.AllPick, LobbyType.Ranked, "Any");
  }

  public onClickGo() : void {
    this.setupNewReport();

    this.openDotaService.getMatches(this.formModel.steamID)
      .subscribe((matches: IMatch[]) => {
        this.getWinrates(matches)
      });
  }

  public getLobbyTypeName(lobbyType : string | LobbyType) : string{
    return typeof lobbyType === "number" ? LobbyType[lobbyType] : lobbyType as string;
  }

  public getGameModeName(gameMode : string | GameMode) : string{
    return typeof gameMode === "number" ? GameMode[gameMode] : gameMode as string;
  }

  private getWinrates(matches: IMatch[]) : void {
    this.totalMatchesIncluded = 0;
    this.totalMatchesPlayed = matches.length;

    matches.forEach((matchData: IMatch) => {
      let match: Match = Object.assign(new Match(), matchData);

      if (this.shouldIncludeMatch(match)){
        const day: number = match.date().getDay();
        this.dayReports[day].matches.push(match);
        this.totalMatchesIncluded++;
      }
    });

    for (let i = 0; i < 7; i++) {
      this.dayReports[i].getWinrates();
    }

    this.showTable = true;
  }

  private shouldIncludeMatch(match: Match) : boolean {
    let shouldInclude : boolean = true;

    let selectedGameMode : string | GameMode = this.formModel.gameMode;
    let selectedLobbyType : string | LobbyType = this.formModel.lobbyType;
    let selectedPartySize : string | number = this.formModel.partySize;

    if (selectedGameMode != "Any" && selectedGameMode != match.game_mode){
      shouldInclude = false;
    }

    if (selectedLobbyType != "Any" && selectedLobbyType != match.lobby_type) {
      shouldInclude = false;
    }

    // edge cases

    // if game mode didn't match, check if they really wanted ranked all pick, if it matches, include it
    if (selectedGameMode == GameMode.AllPick && selectedLobbyType == LobbyType.Ranked && match.game_mode == GameMode.RankedAllPick && match.lobby_type == LobbyType.Ranked){
      shouldInclude = true;
    }

    // if lobby type is any and user selected all pick, include matches that are all pick and ranked all pick
    if (selectedLobbyType == "Any" && selectedGameMode == GameMode.AllPick && (match.game_mode == GameMode.AllPick || match.game_mode == GameMode.RankedAllPick)){
      shouldInclude = true;
    }

    // Newer normal all pick matches show up as lobbytype = normal and gamemode = rankedallpick, account for that here
    if (selectedGameMode == GameMode.AllPick && selectedLobbyType == LobbyType.Normal && match.lobby_type == LobbyType.Normal &&
      (match.game_mode == GameMode.AllPick || match.game_mode == GameMode.RankedAllPick)){
      shouldInclude = true;
    }

    if (selectedPartySize != "Any" && selectedPartySize != match.party_size){
      shouldInclude = false;
    }

    return shouldInclude;
  }

  private setupNewReport() : void{
    for (let i: number = 0; i < 7; i++) {
      this.dayReports[i] = new DayReport(this.days[i]);
    }
  }
}
