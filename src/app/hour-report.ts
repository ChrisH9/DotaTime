import { Match } from './api/match';

export class HourReport {
  public matches: Match[] = [];
  public wins: number = 0;
  public winrate: number = 0;
  public colorType: string;

  public getWinrate(): number {

    this.matches.forEach(match => {
      if (match.didPlayerWin()) {
        this.wins++;
      }
    });

    const totalMatches: number = this.matches.length;
    if (totalMatches > 0) {
      this.winrate = (this.wins / totalMatches) * 100;
    }

    this.setColorType();
    return this.winrate;
  }

  private setColorType() : void {
    if (this.winrate > 80){
      this.colorType = 'superHigh';
    }
    else if (this.winrate > 60){
      this.colorType = 'veryHigh';
    }
    else if (this.winrate > 50){
      this.colorType = 'high';
    }
    else if (this.winrate === 50){
      this.colorType = 'average';
    }
    else if (this.winrate > 40){
      this.colorType = 'low';
    }
    else if (this.winrate > 0) {
      this.colorType = 'veryLow';
    }
    else {
      this.colorType = 'none';
    }
  }
}
