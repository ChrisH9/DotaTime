export enum LobbyType {
  Normal = 0,
  Practice = 1,
  Tournament = 2,
  CoOpWithBots = 4,
  Ranked = 7,
  Solo1Vs1Mid = 8,
  BattleCup = 9
}
